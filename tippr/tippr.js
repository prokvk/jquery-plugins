(function ($) {
    /**
     * config
     */
    var debug = {
        config: 0,
        show_pos: 0
    };
    
    var cfg = {
        attr:       'title',
        position:   'top',    //top|right|bottom|left
        trigger:    'hover',   //hover|click
        plusX:      0,   //custom bubble offset
        plusY:      0,   //custom bubble offset
        animtime:   100   //custom bubble offset
    };
    
    var box_styles = {
        display:            'inline-table',
        height:             18,
        lineHeight:         '18px',
        padding:            '3px 10px 3px 10px',
        backgroundColor:    'black',
        borderRadius:       3,
        color:              'white',
        fontFamily:         'Arial',
        fontSize:           '14px',
        position:           'absolute',
        cursor:             'default'
    };
    
    //Helper Functions
    ////////////////////////////////////////////////////////////////////////////
    function get_abs_pos(elem) {
        var end  = false;
        var prnt = elem;
        var x    = 0;
        var y    = 0;

        while (!end) {
            if (prnt != null) {
                if (prnt.offsetLeft) {
                    x += prnt.offsetLeft;
                }

                if (prnt.offsetTop) {
                    y  += prnt.offsetTop;
                }

                prnt = prnt.offsetParent;
            }//if (
            else {
                break;
            }
        }//while

        return {x: x, y: y};
    };//get_abs_pos
    
    function get_styles_str_from_obj(styles) {
        var str = '';

        //

        var intreg  = new RegExp('^([0-9]+)$');
        var casereg = new RegExp('([A-Z]+)');
        var no_ints = new Array('zIndex','z-index');
        var vals    = [];

        for (var i in styles) {
            var val = styles[i];

            if ((intreg.test(val)) && ($.inArray(i,no_ints) == -1)) {
                val += 'px';
            }

            var key = i.replace(casereg,'-$1').toLowerCase();

            vals[vals.length] = key + ': ' + val + ';';

            //console.log(key,val);
        }//for

        if (vals.length > 0) {
            str = vals.join(' ');
        }

        //

        return str;
    };//get_styles_str_from_obj
    
    function show($item) {
        if (($item.is('.tippr')) && (!$item.is('.tippr_on'))) {
            hide();
            
            //
            
            var pos         = get_abs_pos($item.get(0));
            var height      = $(document).height(); //returns window height
            var offset      = $(document).scrollTop(); //returns scroll position from top of document
            
            //cont
            var cont = '<div class="tippr_box" style="' + get_styles_str_from_obj(box_styles) + '" />';
            
            $('body').append(cont);
            
            var $box = $('.tippr_box');
            
            $box.css('opacity',0).html($item.attr(cfg.attr));
            
            //var styles  = {opacity: 1};
            var styles  = {};
            var width   = $box.outerWidth();
            var height  = $box.outerHeight();
            
            if ((cfg.position == 'top') || (cfg.position == 'bottom')) {
                var factor  = (cfg.position == 'top')? -1 : 1;
                var val     = factor * (height + cfg.plusY);
                
                styles.top  = pos.y + val;
                styles.left = pos.x;
            }//if
            else {
                var factor  = (cfg.position == 'left')? -1 : 1;
                var val     = factor * (width + cfg.plusX);
                
                styles.left = pos.x + val;
                styles.top  = pos.y;
            }//if
            
            $box.css(styles).stop().animate({
                opacity: 1
            },cfg.animtime);
            
            $item.addClass('tippr_on');
            
            //debug
            if (debug.show_pos) {
                console.log('***factor: ',factor);
                console.log('***width: ',width);
                console.log('***plusX: ',cfg.plusX);
                console.log('***val: ',val);
            }//if
        }//if
    };//show
    
    function hide() {
        $('.tippr').removeClass('tippr_on');
        
        $('.tippr_box').remove();
    };//hide
    
    //jQuery extend
    ////////////////////////////////////////////////////////////////////////////
    $.fn.tippr = function(opts) {
        return $(this).each(function () {
            var $this = $(this);
            
            cfg = $.extend({},cfg,opts);
        
            if (debug.config) {
                console.log('***config: ',cfg);
            }

            //init elem
            $this.addClass('tippr');
            
            if (cfg.trigger == 'hover') {
                $('body').mousemove(function (e) {
                    var $this = $(e.target);
                    
                    if ($this.is('.tippr')) {
                        show($this);
                    }
                    else {
                        if (!$this.is('.tippr_box')) {
                            hide();
                        }
                    }//else
                });
            }//if
            else if (cfg.trigger == 'click') {
                $this.click(function () {
                    if ($this.is('.tippr_on')) {
                        hide();
                    }
                    else {
                        show($this);
                    }
                    
                    return false;
                });
            }//else if
        });
    }//$.tippr
})(jQuery);