(function ($) {
    /**
     * config
     */
    var debug = {
        config: 0,
        type: 0,
        update: 1
    };
    
    var cfg = {
        onUpdate: function (pct,items_done,items_total) {}
    };
    
    var wrap_styles = {
        display:    'block',
        opacity:    0,
        width:      0,
        height:     0,
        position:   'absolute',
        left:       '-9999em',
        top:        '-9999em'
    };
    
    var user_sources    = [];
    var $wrap           = null;
    var items_done      = 0;
    var items_total     = 0;
    var pct_done        = 0;
    
    //Helper Functions
    ////////////////////////////////////////////////////////////////////////////
    function preload() {
        //create, just once
        if (!$('.preloard_wrapper').get(0)) {
            $('body').append('<div class="preloard_wrapper" style="' + get_styles_str_from_obj(wrap_styles) + '" />');
        }
        
        $wrap = $('.preloard_wrapper');
        
        //
        items_total = user_sources.length;
        
        for (var i in user_sources) {
            var cn = 'item' + (i + 1);
            
            $wrap.append('<img class="' + cn + '" src="' + user_sources[i] + '" />');
            
            $wrap.find('img.' + cn).load(function () {
                items_done++;
                
                pct_done = parseInt((items_done / items_total) * 100,10);
                
                //
                cfg.onUpdate(pct_done,items_done,items_total);
                
                if (debug.update) {
                    console.log('***update: ' + pct_done + '% done (' + items_done + ' / ' + items_total + ')');
                }
            });
        }//for
    };//preload
    
    function get_styles_str_from_obj(styles) {
        var str = '';

        //

        var intreg  = new RegExp('^([0-9]+)$');
        var casereg = new RegExp('([A-Z]+)');
        var no_ints = new Array('zIndex','z-index');
        var vals    = [];

        for (var i in styles) {
            var val = styles[i];

            if ((intreg.test(val)) && ($.inArray(i,no_ints) == -1)) {
                val += 'px';
            }

            var key = i.replace(casereg,'-$1').toLowerCase();

            vals[vals.length] = key + ': ' + val + ';';
        }//for

        if (vals.length > 0) {
            str = vals.join(' ');
        }

        //

        return str;
    };//get_styles_str_from_obj
    
    //jQuery extend
    ////////////////////////////////////////////////////////////////////////////
    $.preloadr = function(src,opts) {
        //config
        cfg = $.extend({},cfg,opts);
        
        //debug
        if (debug.config) {
            console.log('***config: ',cfg);
        }
        
        if (debug.type) {
            console.log('***type: ',typeof(src));
        }
        
        //queue
        if (typeof(src) == 'string') {
            user_sources[user_sources.length] = src;
        }
        else if (typeof(src) == 'object') {
            for (var i in src) {
                user_sources[user_sources.length] = src[i];
            }
        }//else if
        
        //preload
        preload();
    };//$.preloadr
})(jQuery);