(function ($) {
    /**
     * config
     */
    var debug = {
        combo_log: 0,
        eval_log: 0,
        keys_used: 0,
        combo_trigger: 1
    };
    
    /**
     * init..
     */
    var keys = {};
    
    var keys_win = {
        ctrl:   17,
        alt:    18,
        shift:  16,
        enter:  13,
        space:  32,
        esc:    27,
        a:      65,
        zero:   96,
        down:   40,
        up:     38,
        left:   37,
        right:  39
    };
    
    var keys_mac = {
        ctrl:   91,
        alt:    17,
        shift:  16,
        enter:  13,
        space:  32,
        esc:    27,
        a:      65,
        zero:   96,
        down:   40,
        up:     38,
        left:   37,
        right:  39
    };
    
    var current_combo   = [];
    var user_combos     = [];
    
    //
    set_keys();
    
    //bing keys
    $(window).keydown(function (e) {
        combo_add(e);
    });
    
    $(window).keyup(function (e) {
        combo_delete(e);
    });
    
    //Helper Functions
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * bind keystroke to callback
     */
    function bind_combo(combo,callback) {
        user_combos[user_combos.length] = {
            combo: combo,
            callback: callback
        };
    };//bind_combo
    
    /**
     * validation process
     */
    function validate_combos() {
        for (var x = 0; x < user_combos.length; x++) {
            var ok          = false;
            var combo       = user_combos[x].combo;
            var callback    = user_combos[x].callback;
        
            //get clean combo value (lowercase, spaces ...)
            var parts = combo.split('+');

            for (var i in parts) {
                parts[i] = $.trim(parts[i]);
            }

            var clean = parts.join('+').toLowerCase();

            parts = clean.split('+');

            if (debug.eval_log) {
                console.log('***',current_combo.length + ',' + parts.length);
            }

            if (current_combo.length >= parts.length) {
                ok = true;

                var vals = [];

                for (i in parts) {
                    var val     = 0;
                    var item    = parts[i];

                    if ((typeof(keys[item]) != 'undefined') && (item != 'a')) {
                        val = keys[item];
                    }
                    else {
                        val = item.charCodeAt(0);
                    }

                    if ($.inArray(val,current_combo) == -1) {
                        ok = false;

                        break;
                    }//if
                }//for
            }//if

            if (ok) {
                if (debug.combo_trigger) {
                    console.log('***launching combo:',combo);
                }
                
                if (typeof(callback) == 'function') {
                    callback();
                }
            }
        }//for
    };//validate_combo
    
    /**
     * key codes based on platform
     */
    function set_keys() {
        var os = navigator.platform.toLowerCase();
        
        if (os.indexOf('mac') == -1) {
            keys = keys_win;
        }
        else {
            keys = keys_mac;
        }
        
        if (debug.keys_used) {
            var name = (keys == keys_mac)? 'MAC' : 'WIN';
            
            console.log('***setting keys: ',name);
        }//if
    };//set_keys
    
    /**
     * add key to list for keystroke validation
     */
    function combo_add(event) {
        if ($.inArray(event.keyCode,current_combo) == -1) {
            current_combo[current_combo.length] = event.keyCode;
        }
        
        validate_combos();
        
        if (debug.combo_log) {
            console.log('***added: ',current_combo.join(' + '));
        }
    };//combo_add
    
    /**
     * remove key from list
     */
    function combo_delete(event) {
        for (var i in current_combo) {
            if (current_combo[i] == event.keyCode) {
                current_combo.splice(i,1);
                
                break;
            }//if
        }//for
        
        if (debug.combo_log) {
            console.log('***removed "' + event.keyCode + '": ',current_combo.join(' + '));
        }
    };//combo_add
    
    //jQuery extend
    ////////////////////////////////////////////////////////////////////////////
    $.keystroke_bind = function(combo,callback) {
        bind_combo(combo,callback);
    }
})(jQuery);